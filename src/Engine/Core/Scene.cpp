/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Scene.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include <GLFW/glfw3.h>
#include <glm/gtc/constants.hpp>
#include <glm/ext.hpp>
#include "Scene.h"

Scene::~Scene() {
    _entities.clear();
}

void Scene::update() {
    static_cast<float>(glfwGetTime());
    for (auto & entity : _entities) {
        if (entity->isEnabled()) {
            entity->update();
        }
    }
}

void Scene::draw() {
    glClearColor(_color.r, _color.g, _color.b, _color.a);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
    for (auto & entity : _entities) {
        if (entity->isEnabled()) {
            entity->draw();
        }
    }
}

void Scene::addEntity(Entity * entity) {
    _entities.push_front(entity);
}

void Scene::setView(const glm::mat4 & view) {
    _view = view;
}

const glm::mat4 & Scene::getView() const {
    return _view;
}

void Scene::setProj(const glm::mat4 &proj) {
    _proj = proj;
}

const glm::mat4 &Scene::getProj() const {
    return _proj;
}

void Scene::setAspect(double aspect) {
    const auto pi = glm::pi<float>();
    glm::mat4 proj = glm::perspective(pi/2., aspect, 0.1, 100.);
    setProj(proj);
}

void Scene::setSceneManager(SceneManager *sceneManager) {
    _sceneManager = sceneManager;
}

SceneManager *Scene::getSceneManager() {
    return _sceneManager;
}

InputHandler *Scene::getInputHandler() const {
    return _inputHandler;
}

void Scene::setInputHandler(InputHandler *inputHandler) {
    Scene::_inputHandler = inputHandler;
}

void Scene::setAudioPlayer(AudioPlayer *audioPlayer) {
    _audioPlayer = audioPlayer;
}

AudioPlayer *Scene::getAudioPlayer() {
    return _audioPlayer;
}

Scene::Scene(const glm::vec4 &color) : _color(color) {}


