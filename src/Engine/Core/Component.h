/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Component.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef COMPONENT_H
#define COMPONENT_H

class Entity;

class Component
{
public:
    explicit Component(Entity* entity);
    virtual ~Component() = default;
    virtual void update() = 0;
    virtual void draw() = 0;
    virtual void processKey(int);
    bool isEnabled();
    void setEnabled(bool);
    void setEntity(Entity * entity);

protected:
    Entity *_entity;

private:
    bool _enabled;
};

#endif // COMPONENT_H
