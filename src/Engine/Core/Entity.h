/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Entity.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef ENTITY_H
#define ENTITY_H

#include <list>
#include <glm/vec3.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <iostream>
#include <unordered_map>

class Component;
class Scene;

class Entity
{
public:
    Entity();
    explicit Entity(Scene * scene);
    ~Entity();
    void addComponent(Component*);
    virtual void update();
    virtual void draw();
    template<typename T> T * getComponent();
    bool isEnabled();
    void setEnabled(bool);
    Scene * getScene();

protected:
    Scene *_scene;

private:
    std::unordered_map<const std::type_info*, Component*> _components;
    bool _enabled;
};

template<typename T> T * Entity::getComponent() {
    auto kv = _components.find(&typeid(T));
    if ( kv != _components.end()) {
        return (T*)kv->second;
    }
    return nullptr;
}
#endif // ENTITY_H
