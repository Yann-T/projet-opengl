/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Entity.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "Entity.h"
#include "Component.h"

Entity::Entity() : _scene(nullptr), _enabled(true) {

}

Entity::Entity(Scene * scene) : _scene(scene), _enabled(true) {

}

Entity::~Entity() {
    _components.clear();
}

void Entity::update() {
    for (auto & component : _components) {
        if (component.second->isEnabled()) {
            component.second->update();
        }
    }
}

void Entity::draw() {
    for (auto & component : _components) {
        if (component.second->isEnabled()) {
            component.second->draw();
        }
    }
}

void Entity::addComponent(Component * component) {
    _components[&typeid(*component)] = component;
}

bool Entity::isEnabled() {
    return _enabled;
}

void Entity::setEnabled(bool newState) {
    _enabled = newState;
}

Scene *Entity::getScene() {
    return _scene;
}

