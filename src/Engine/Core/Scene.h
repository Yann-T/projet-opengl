/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Scene.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_SCENE_H
#define OPENGL_PROJECT_SCENE_H


#include <list>
#include <glm/detail/type_mat4x4.hpp>
#include <functional>
#include <Engine/AudioPlayer.h>
#include "Entity.h"
#include "Engine/InputHandler.h"

class SceneManager;

class Scene {
public:
    Scene(const glm::vec4 & color = {0.7, 0.7, 0.7, 1});
    virtual ~Scene();
    virtual void preload() = 0;
    virtual void initialize() = 0;
    virtual void update();
    virtual void draw();
    void addEntity(Entity*);
    void setView(const glm::mat4 & view);
    const glm::mat4 & getView() const;
    void setProj(const glm::mat4 & proj);
    const glm::mat4 & getProj() const;
    void setAudioPlayer(AudioPlayer * audioPlayer);
    void setAspect(double aspect);
    void setSceneManager(SceneManager * sceneManager);
    void setInputHandler(InputHandler *inputHandler);
    SceneManager * getSceneManager();
    InputHandler * getInputHandler() const;
    AudioPlayer * getAudioPlayer();

protected:
    std::list<Entity*> _entities;

private:
    SceneManager * _sceneManager{};
    InputHandler * _inputHandler{};
    AudioPlayer * _audioPlayer{};

private:
    glm::mat4 _view{};
    glm::mat4 _proj{};
    glm::vec4 _color;
};


#endif //OPENGL_PROJECT_SCENE_H
