/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file InputHandler.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */


#include "InputHandler.h"

void InputHandler::handleKeyInput(int key) {
    if(!_enabled) {
        return;
    }
    auto range = _keysForComponent.equal_range(key);
    for(auto e = range.first;  e != range.second; e++) {
        e->second->processKey(key);
    }
}

void InputHandler::registerComponent(Component *component, std::list<int> keys) {
    for(int key: keys) {
        _keysForComponent.insert(std::make_pair(key, component));
    }
}

void InputHandler::setEnabled(bool enabled) {
    _enabled = enabled;
}

