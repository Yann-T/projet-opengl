/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file AudioPlayer.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include <soloud_wav.h>
#include <iostream>
#include "AudioPlayer.h"

AudioPlayer::AudioPlayer() {
    _soloud.init();
}

AudioPlayer::~AudioPlayer() {
    _soloud.deinit();
    for (auto & registeredEffect : _registeredEffects) {
        delete registeredEffect.second;
    }
}

void AudioPlayer::playSoundEffect(const std::string& name) {
    auto it = _registeredEffects.find(name);
    if(it != _registeredEffects.end()) {
        SoLoud::Wav&  currentEffect = *(it->second);
        _soloud.play(currentEffect);
    } else {
        std::cerr << "The sound effect does not exist" << std::endl;
    }
}

void AudioPlayer::addSoundEffect(const std::string &filename, const std::string& name) {
    auto * sound = new SoLoud::Wav();
    auto res = sound->load(filename.c_str());
    if(!(res == SoLoud::SO_NO_ERROR)) {
        std::cerr << "Error loading file : " << res << std::endl;
    }
    auto it = _registeredEffects.find(name);
    if(it != _registeredEffects.end()) {
        delete _registeredEffects[name];
    }
    _registeredEffects[name] = sound;
}

