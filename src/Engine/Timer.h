/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Timer.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_TIMER_H
#define OPENGL_PROJECT_TIMER_H


#include "Core/Entity.h"
#include "Core/Scene.h"

class Timer : public Entity {
public:
    explicit Timer(Scene * parent, double startTime);
    void update() override;
    void start();
    void stop();
    bool isFinished();

    bool oneSecondHasPassed();

private:
    double _currentTime;
    double _previousTime;
    bool _finished;
    bool _secondPassedFlag;
    int _lastSecondPassed;
};


#endif //OPENGL_PROJECT_TIMER_H
