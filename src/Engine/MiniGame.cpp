/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file MiniGame.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "MiniGame.h"
#include "Timer.h"
#include "HUD/HUD.hpp"

MiniGame::MiniGame(double timerDuration, const glm::vec4 & backgroundColor) : Scene(backgroundColor), _timerDuration(timerDuration), _hud() {

}

void MiniGame::initialize() {
    _timer = new Timer(this, _timerDuration);
    _hud = new HUD(this);
    _hud->setTimer((int)round(_timerDuration));
    addEntity(_timer);
    addEntity(_hud);
}

void MiniGame::update() {
    Scene::update();
    if(_timer->isFinished()) {
        onTimerFinished();
    } else if(_timer->oneSecondHasPassed()) {
        _hud->decreaseTimer();
    }
}
