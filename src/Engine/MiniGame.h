/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file MiniGame.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_MINIGAME_H
#define OPENGL_PROJECT_MINIGAME_H

#include "Core/Scene.h"

class Timer;
class HUD;

class MiniGame : public Scene {
public:
    MiniGame(double timerDuration, const glm::vec4 & backgroundColor = {0.7, 0.7, 0.7, 1});
    virtual void initialize() override;
    void update() override;

    virtual void onTimerFinished() = 0;
    virtual void onWin() = 0;
    virtual void onFailed() = 0;

private:
    double _timerDuration;
    Timer * _timer{};

protected:
    HUD * _hud;
};


#endif //OPENGL_PROJECT_MINIGAME_H
