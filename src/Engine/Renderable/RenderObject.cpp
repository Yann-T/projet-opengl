/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file RenderObject.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include <ObjLoader.hpp>
#include "RenderObject.h"
#include "glm/gtc/matrix_transform.hpp"


RenderObject::RenderObject(const glm::mat4 & modelWorld) : m_mw(modelWorld)
{
    m_diffusemap = std::unique_ptr<Sampler>(new Sampler(0));
    m_normalmap = std::unique_ptr<Sampler>(new Sampler(1));
    m_specularmap = std::unique_ptr<Sampler>(new Sampler(2));
    m_color = glm::vec4(1);
}

void RenderObject::draw()
{
    m_diffusemap->bind();
    m_normalmap->bind();
    m_specularmap->bind();
    for (auto & part : m_parts) {
        part.draw(m_diffusemap.get(), m_normalmap.get(), m_specularmap.get(), m_color);
    }
    m_diffusemap->unbind();
    m_normalmap->unbind();
    m_specularmap->unbind();
}

void RenderObject::update(const glm::mat4 & proj, const glm::mat4 & view)
{
    for (auto & part : m_parts) {
        part.update(proj, view, m_mw, false);
    }
}

void RenderObject::translate(const glm::vec3 vec) {
    this->m_mw = glm::translate(m_mw, vec);
}

std::unique_ptr<RenderObject> RenderObject::createWavefrontInstance(const std::string & objname, const glm::mat4 & modelWorld)
{
    std::unique_ptr<RenderObject> object(new RenderObject(modelWorld));
    object->loadWavefront(objname);
    return object;
}

void RenderObject::setProgramMaterial(std::shared_ptr<Program> & program, const SimpleMaterial & material) const
{
    program->bind();
    program->setUniform("lightsInWorld[0].direction", glm::normalize(glm::vec3(0, 1, 0)));
    program->setUniform("lightsInWorld[0].intensity", glm::vec3(0.7, 0.7, 0.7));
    program->setUniform("lightsInWorld[1].direction", glm::normalize(glm::vec3(0, 0, 1)));
    program->setUniform("lightsInWorld[1].intensity", glm::vec3(0.5, 0.5, 0.5));
    program->setUniform("lightsInWorld[2].direction", glm::normalize(glm::vec3(-1, 0, -1)));
    program->setUniform("lightsInWorld[2].intensity", glm::vec3(0.6, 0.6, 0.6));
    program->setUniform("material.ambient", material.ambient);
    program->setUniform("material.diffuse", material.diffuse);
    program->setUniform("material.specular", material.specular);
    program->setUniform("material.shininess", material.shininess);
    m_diffusemap->attachToProgram(*program, "material.colormap", Sampler::DoNotBind);
    m_normalmap->attachToProgram(*program, "material.normalmap", Sampler::DoNotBind);
    m_specularmap->attachToProgram(*program, "material.specularmap", Sampler::DoNotBind);
    program->unbind();
}

void RenderObject::loadWavefront(const std::string & objname)
{
    ObjLoader objLoader(objname);
    const std::vector<SimpleMaterial> & materials = objLoader.materials();
    std::vector<glm::vec3> vertexPositions = objLoader.vertexPositions();
    const std::vector<glm::vec2> & vertexUVs = objLoader.vertexUVs();
    std::vector<glm::vec3> vertexNormals = objLoader.vertexNormals();
    std::vector<glm::vec3> vertexTangents = objLoader.vertexTangents();
    // set up the VBOs of the master VAO
    std::shared_ptr<VAO> vao(new VAO(4));
    vao->setVBO(0, vertexPositions);
    vao->setVBO(1, vertexUVs);
    vao->setVBO(2, vertexNormals);
    vao->setVBO(3, vertexTangents);
    size_t nbParts = objLoader.nbIBOs();
    for (size_t k = 0; k < nbParts; k++) {
        const std::vector<uint> & ibo = objLoader.ibo(static_cast<unsigned int>(k));
        if (ibo.size() == 0) {
            continue;
        }
        std::shared_ptr<VAO> vaoSlave;
        vaoSlave = vao->makeSlaveVAO();
        vaoSlave->setIBO(ibo);

        std::shared_ptr<Program> program(new Program("shaders/simplemat.v.glsl", "shaders/simplemat.f.glsl"));
        const SimpleMaterial & material = materials[k];
        setProgramMaterial(program, material);
        Image<> colorMap = objLoader.image(material.diffuseTexName);
        std::shared_ptr<Texture> texture(new Texture(GL_TEXTURE_2D));
        texture->setData(colorMap);
        Image<> normalMap = objLoader.image(material.normalTexName);
        std::shared_ptr<Texture> ntexture(new Texture(GL_TEXTURE_2D));
        ntexture->setData(normalMap);
        Image<> specularMap = objLoader.image(material.specularTexName);
        std::shared_ptr<Texture> stexture(new Texture(GL_TEXTURE_2D));
        stexture->setData(specularMap);
        m_parts.emplace_back(vaoSlave, program, texture, ntexture, stexture);
    }
    m_diffusemap->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_diffusemap->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_diffusemap->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_diffusemap->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
    m_normalmap->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_normalmap->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_normalmap->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_normalmap->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
    m_specularmap->setParameter(GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    m_specularmap->setParameter(GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    m_specularmap->setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    m_specularmap->setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
}