/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file RenderObjectPart.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "RenderObjectPart.h"

RenderObjectPart::RenderObjectPart(std::shared_ptr<VAO> vao, std::shared_ptr<Program> program, std::shared_ptr<Texture> texture, std::shared_ptr<Texture> ntexture,
                                                   std::shared_ptr<Texture> stexture)
        : m_vao(vao), m_program(program), m_diffuseTexture(texture), m_normalTexture(ntexture), m_specularTexture(stexture)
{
}

void RenderObjectPart::draw(Sampler * colormap, Sampler * normalmap, Sampler * specularmap, const glm::vec4 & color)
{
    m_program->bind();
    colormap->attachTexture(*m_diffuseTexture);
    normalmap->attachTexture(*m_normalTexture);
    specularmap->attachTexture(*m_specularTexture);
    m_program->setUniform("color", color);
    m_vao->draw();
    m_program->unbind();
}

void RenderObjectPart::update(const glm::mat4 & proj, const glm::mat4 & view, const glm::mat4 & mw, bool displayNormals)
{
    m_program->bind();
    m_program->setUniform("M", mw);
    m_program->setUniform("V", view);
    m_program->setUniform("P", proj);
    m_program->setUniform("positionCameraInWorld", glm::vec3(glm::inverse(view) * glm::vec4(0, 0, 0, 1)));
    if (displayNormals) {
        m_program->setUniform("displayNormals", 1);
    } else {
        m_program->setUniform("displayNormals", 0);
    }
    m_program->unbind();
}