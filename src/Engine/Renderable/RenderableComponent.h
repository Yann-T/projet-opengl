/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file RenderableComponent.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */


#ifndef OPENGL_PROJECT_RENDERABLECOMPONENT_H
#define OPENGL_PROJECT_RENDERABLECOMPONENT_H

#include <vector>
#include <memory>
#include <glApi.hpp>
#include "Engine/Core/Component.h"
#include "RenderObjectPart.h"
#include "RenderObject.h"

class RenderableComponent : public Component {
public:
    RenderableComponent(Entity *entity, const std::string & objectPath, const glm::mat4 & mat);
    void update() override;
    void draw() override;

    void translate(const glm::vec3 &vec);

private:
    std::unique_ptr<RenderObject> _renderObject;
};


#endif //OPENGL_PROJECT_RENDERABLECOMPONENT_H
