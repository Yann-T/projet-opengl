/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file RenderObjectPart.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_RENDEROBJECTPART_H
#define OPENGL_PROJECT_RENDEROBJECTPART_H

#include <glApi.hpp>
#include <memory>

class RenderObjectPart {
public:
    RenderObjectPart() = delete;
    RenderObjectPart(const RenderObjectPart &) = delete;
    RenderObjectPart(RenderObjectPart &&) = default;
    RenderObjectPart(std::shared_ptr<VAO> vao, std::shared_ptr<Program> program, std::shared_ptr<Texture> texture, std::shared_ptr<Texture> ntexture, std::shared_ptr<Texture> stexture);
    void draw(Sampler * colormap, Sampler * normalmap, Sampler * specularmap, const glm::vec4 & color);
    void update(const glm::mat4 & proj, const glm::mat4 & view, const glm::mat4 & mw, bool displayNormals);

private:
    std::shared_ptr<VAO> m_vao;
    std::shared_ptr<Program> m_program;
    std::shared_ptr<Texture> m_diffuseTexture;
    std::shared_ptr<Texture> m_normalTexture;
    std::shared_ptr<Texture> m_specularTexture;
};


#endif //OPENGL_PROJECT_RENDEROBJECTPART_H
