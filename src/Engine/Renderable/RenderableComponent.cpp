/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file RenderableComponent.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "RenderableComponent.h"
#include "Engine/Core/Scene.h"

RenderableComponent::RenderableComponent(Entity *entity, const std::string & objectPath, const glm::mat4 & mw) : Component(entity) {
    _renderObject = RenderObject::createWavefrontInstance(objectPath, mw);
}

void RenderableComponent::draw() {
    _renderObject->draw();
}

void RenderableComponent::update() {
    Scene * scene = _entity->getScene();
    _renderObject->update(scene->getProj(), scene->getView());
}


void RenderableComponent::translate(const glm::vec3 &vec) {
    _renderObject->translate(vec);
}