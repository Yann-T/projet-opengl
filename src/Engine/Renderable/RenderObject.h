/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file RenderObject.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_RENDEROBJECT_H
#define OPENGL_PROJECT_RENDEROBJECT_H

#include <memory>
#include <glApi.hpp>
#include <SimpleMaterial.hpp>
#include "RenderObjectPart.h"

/**
   * @brief The RenderObject class
   *
   * A RenderObject is split into parts, sharing the same geometry (VBOs), but referencing different primitive subsets (IBO) and materials (textures, ...).
   */
class RenderObject {
public:
    RenderObject() = delete;
    RenderObject(const RenderObject &) = delete;
    /**
     * @brief creates an instance from a wavefront file and modelWorld matrix
     * @param objname the filename of the wavefront file
     * @param modelWorld the matrix transform between the object (a.k.a model) space and the world space
     * @return the created RenderObject as a smart pointer
     */
    static std::unique_ptr<RenderObject> createWavefrontInstance(const std::string & objname, const glm::mat4 & modelWorld);

    /**
     * @brief Sets all uniform variables related to material and lighting
     * @param program
     * @param material
     *
     * @note Besides the material, three directional lights (defined in world space) are passed to the GLSL program.
     */
    void setProgramMaterial(std::shared_ptr<Program> & program, const SimpleMaterial & material) const;

    /**
     * @brief Draw this RenderObject
     */
    void draw();

    /**
     * @brief update the program MVP uniform variable
     * @param proj the projection matrix
     * @param view the worldView matrix
     * @param mw the model world matrix
     */
    void update(const glm::mat4 & proj, const glm::mat4 & view);


    void translate(const glm::vec3 vec);

private:
    RenderObject(const glm::mat4 & modelWorld);
    void loadWavefront(const std::string & objname);

private:
    glm::mat4 m_mw; ///< modelWorld matrix
    std::vector<RenderObjectPart> m_parts;
    std::unique_ptr<Sampler> m_diffusemap;
    std::unique_ptr<Sampler> m_normalmap;
    std::unique_ptr<Sampler> m_specularmap;
    glm::vec4 m_color;
};


#endif //OPENGL_PROJECT_RENDEROBJECT_H
