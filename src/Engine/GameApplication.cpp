/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file GameApplication.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */


#include <glApi.hpp>
#include <GLFW/glfw3.h>
#include "GameApplication.h"
#include "SceneManager.h"

GameApplication::GameApplication(int windowWidth, int windowHeight) : Application(windowWidth, windowHeight)
{
    _audioPlayer = new AudioPlayer();
    _audioPlayer->addSoundEffect("../sounds/win.wav", "win");
    _audioPlayer->addSoundEffect("../sounds/fail.wav", "fail");
    _audioPlayer->addSoundEffect("../sounds/keyboard.wav", "keyboard");
    _audioPlayer->addSoundEffect("../sounds/sectionfail.wav", "fail2");
    _audioPlayer->addSoundEffect("../sounds/sectionpass.wav", "pass");
    _sceneManager = new SceneManager();
    _aspect = float(windowWidth)/float(windowHeight);
    GLFWwindow * window = glfwGetCurrentContext();
    glfwGetFramebufferSize(window, &windowWidth, &windowHeight);
    resize(window, windowWidth, windowHeight);
    glEnable(GL_BLEND);
    glBlendFuncSeparate(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA, GL_ONE, GL_ZERO);
}

GameApplication::~GameApplication() {
    delete _sceneManager;
    delete _audioPlayer;
}

void GameApplication::resize(GLFWwindow * window, int framebufferWidth, int framebufferHeight) {
    GameApplication &app = *static_cast<GameApplication *>(glfwGetWindowUserPointer(window));
    if (app._sceneManager->getCurrentScene()) {
        float aspect = float(framebufferWidth) / float(framebufferHeight);
        app._aspect = aspect;
        app._sceneManager->getCurrentScene()->setAspect(aspect);
    }
    glViewport(0, 0, framebufferWidth, framebufferHeight);
}

void GameApplication::update() {
    if(_sceneManager->getCurrentScene() != nullptr) {
        _sceneManager->getCurrentScene()->update();
    }
}

void GameApplication::renderFrame() {
    glClearColor(0, 0, 0, 1);
    glClear(GL_COLOR_BUFFER_BIT);
    glClear(GL_DEPTH_BUFFER_BIT);
    if(_sceneManager->getCurrentScene() != nullptr) {
        _sceneManager->getCurrentScene()->draw();
    }
}

void GameApplication::changeScene(const std::string & sceneName) {
    _sceneManager->setAudioPlayer(_audioPlayer);
    _sceneManager->changeScene(sceneName);
}

void GameApplication::setCallbacks() {
  GLFWwindow * window = glfwGetCurrentContext();
  glfwSetFramebufferSizeCallback(window, GameApplication::resize);
  glfwSetKeyCallback(window, GameApplication::keyCallback);

}

float GameApplication::getAspect() {
    GLFWwindow * window = glfwGetCurrentContext();
    GameApplication &app = *static_cast<GameApplication *>(glfwGetWindowUserPointer(window));
    return app._aspect;
}

void GameApplication::keyCallback(GLFWwindow * window, int key, int /*scancode*/, int action, int /*mods*/)
{
    GameApplication & app = *static_cast<GameApplication *>(glfwGetWindowUserPointer(window));
    if(action == GLFW_PRESS) {
        app._sceneManager->getCurrentScene()->getInputHandler()->handleKeyInput(key);
    }
}
