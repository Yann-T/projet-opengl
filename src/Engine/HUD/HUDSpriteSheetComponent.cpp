/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file HUDSpriteSheetComponent.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */


#include "HUDSpriteSheetComponent.hpp"

HUDSpriteSheetComponent::HUDSpriteSheetComponent(Entity *entity, const std::string &spriteSheetPath,
                                                 const glm::vec2 &origin, int gridWidth, int gridHeight)
        : HUDImageComponent(entity, spriteSheetPath, origin), _width(gridWidth), _height(gridHeight), _currentIndex(0)
{
    setImageAspect(float(_img.width)/float(_width), float(_img.height)/float(_height));
    setImagePosition(origin);
    setImageUVs({{0, 1./_height},
                 {0, 0},
                 {1./_width, 0},
                 {1./_width, 1./_height}});
}

void HUDSpriteSheetComponent::getSpriteAtIndex(int index) {
    _currentIndex = index;
    auto x = float(index%_width);
    auto y = float(index/_width);
    auto width = float(_width);
    auto height = float(_height);
    setImageUVs({{x/width, (y+1.)/height},
                 {x/width, y/height},
                 {(x+1.)/width, y/height},
                 {(x+1.)/width, (y+1.)/height}});
}

void HUDSpriteSheetComponent::next() {
    getSpriteAtIndex(++_currentIndex);
}

void HUDSpriteSheetComponent::previous() {
    getSpriteAtIndex(--_currentIndex);
}
