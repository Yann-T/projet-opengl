/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file HUD.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "HUD.hpp"
#include "Engine/Core/Scene.h"
#include "HUDTimer.hpp"

HUD::HUD(Scene * scene) : Entity(scene) {
    auto * timer = new HUDTimer(this);
    timer->setSize(0.3);
    addComponent(timer);
}

void HUD::decreaseTimer() {
    getComponent<HUDTimer>()->previous();
}

void HUD::setTimer(int sec) {
    getComponent<HUDTimer>()->getSpriteAtIndex(sec);
}