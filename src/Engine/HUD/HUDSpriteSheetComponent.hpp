/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file HUDSpriteSheetComponent.hpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_HUDSPRITESHEETCOMPONENT_HPP
#define OPENGL_PROJECT_HUDSPRITESHEETCOMPONENT_HPP


#include "HUDImageComponent.hpp"
#include <string>
#include <glm/vec2.hpp>

class HUDSpriteSheetComponent : public HUDImageComponent {
public:
    HUDSpriteSheetComponent(Entity * entity, const std::string & spriteSheetPath, const glm::vec2 & origin,
            int gridWidth, int gridHeight);
    void getSpriteAtIndex(int index);
    void next();
    void previous();

private:
    int _width;
    int _height;
    int _currentIndex;
};


#endif //OPENGL_PROJECT_HUDSPRITESHEETCOMPONENT_HPP
