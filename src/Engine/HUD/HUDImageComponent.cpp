/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file HUDImageComponent.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "HUDImageComponent.hpp"
#include "Engine/GameApplication.h"
#include <stb_image.h>
#include <utils.hpp>

HUDImageComponent::HUDImageComponent(Entity *entity, const std::string &imgPath, const glm::vec2 &origin)
        : Component(entity),  _img(), _imgPath(imgPath), _origin(), _size(1),
        _program("shaders/hud.v.glsl", "shaders/hud.f.glsl"), _rectangle(2), _sampler(0) {
    _program.bind();
    _sampler.bind();
    _sampler.setParameter(GL_TEXTURE_WRAP_S, GL_REPEAT);
    _sampler.setParameter(GL_TEXTURE_WRAP_T, GL_REPEAT);
    _sampler.unbind();
    _img.data = stbi_load(absolutename(imgPath).c_str(), &_img.width, &_img.height, &_img.channels, STBI_default);
    _program.unbind();

    setImageAspect((float)_img.width, (float)_img.height);
    setImagePosition(origin);
    setImageUVs({{0, 1}, {0, 0}, {1, 0}, {1, 1}});
    std::vector<uint> ibo = {0, 1, 2, 0, 2, 3};
    _rectangle.setIBO(ibo);
}

void HUDImageComponent::update() {
    _program.bind();
    _program.setUniform("aspect", GameApplication::getAspect());
    _program.setUniform("origin", _origin);
    _program.setUniform("size", _size);
    _program.unbind();

}

void HUDImageComponent::draw() {
    Texture texture(GL_TEXTURE_2D);
    texture.setData(_img, true);
    _program.bind();
    _sampler.bind();
    _sampler.attachToProgram(_program, "colormap", Sampler::DoNotBind);
    _sampler.attachTexture(texture);
    _rectangle.draw();
    _sampler.unbind();
    _program.unbind();
}

void HUDImageComponent::setImagePosition(const glm::vec2 &origin) {
    _origin = origin;
}

void HUDImageComponent::setImageUVs(const std::vector<glm::vec2> &vertexUVs) {
    _rectangle.setVBO(1, vertexUVs);
}

void HUDImageComponent::setImageAspect(float width, float height) {
    glm::vec2 aspect;
    if (width > height) {
        aspect = {1, height / width};
    } else {
        aspect = {width / height, 1};
    }
    std::vector<glm::vec2> positions = {{-aspect.x/2 , -aspect.y/2},
                                        {-aspect.x/2, aspect.y/2},
                                        {aspect.x/2, aspect.y/2},
                                        {aspect.x/2, -aspect.y/2}};
    _rectangle.setVBO(0, positions);
}

void HUDImageComponent::setSize(float size) {
    _size = size;
}

