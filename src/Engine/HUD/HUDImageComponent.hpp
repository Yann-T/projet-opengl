/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file HUDImageComponent.hpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */


#ifndef OPENGL_PROJECT_HUDIMAGECOMPONENT_HPP
#define OPENGL_PROJECT_HUDIMAGECOMPONENT_HPP

#include "Engine/Core/Component.h"
#include <string>
#include <vector>
#include "glApi.hpp"

class Entity;
class HUDImageComponent : public Component {
public:
    HUDImageComponent(Entity * entity, const std::string & imgPath, const glm::vec2 &origin);
    void update() override;
    void draw() override;
    void setImagePosition(const glm::vec2 &origin);
    void setImageUVs(const std::vector<glm::vec2> &vertexUVs);
    void setImageAspect(float width, float height);
    void setSize(float size);

protected:
    Image<> _img;

private:
    std::string _imgPath;
    glm::vec2 _origin;
    float _size;
    Program _program;
    VAO _rectangle;
    Sampler _sampler;
};
#endif //OPENGL_PROJECT_HUDIMAGECOMPONENT_HPP
