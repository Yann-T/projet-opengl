/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file GameApplication.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_GAMEAPPLICATION_H
#define OPENGL_PROJECT_GAMEAPPLICATION_H


#include <Application.hpp>
#include "Core/Scene.h"

class GameApplication : public Application{
public:
    GameApplication(int windowWidth, int windowHeight);
    ~GameApplication();

    void changeScene(const std::string &);
    void setCallbacks() override;

    static float getAspect();

protected:
    SceneManager * _sceneManager;

private:
    void update() override;
    void renderFrame() override;

    AudioPlayer * _audioPlayer;
    float _aspect;

    static void resize(GLFWwindow *window, int framebufferWidth, int framebufferHeight);
    static void keyCallback(GLFWwindow * window, int key, int /*scancode*/, int action, int /*mods*/);
};


#endif //OPENGL_PROJECT_GAMEAPPLICATION_H
