/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file SceneManager.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "SceneManager.h"
#include "GameApplication.h"

SceneManager::~SceneManager() {
    delete _currentScene;
}

void SceneManager::changeScene(Scene *newScene) {
    delete _currentScene;
    _currentScene = newScene;
    _currentScene->setAudioPlayer(_audioPlayer);
    _currentScene->setSceneManager(this);
    _currentScene->setAspect(GameApplication::getAspect());
    _currentScene->initialize();
}

Scene *SceneManager::getCurrentScene() {
    return _currentScene;
}

void SceneManager::setAudioPlayer(AudioPlayer *audioPlayer) {
    _audioPlayer = audioPlayer;
}

void SceneManager::addScene(const std::string &name, Scene *scene) {
    _scenes[name] = scene;
}

void SceneManager::changeScene(const std::string &name) {
    changeScene(_scenes[name]);
}
