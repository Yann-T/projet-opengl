/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Timer.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include <GLFW/glfw3.h>
#include "Timer.h"

Timer::Timer(Scene * parent, double startTime) : Entity(parent), _currentTime(startTime) {
    _previousTime = glfwGetTime();
    _finished = false;
    _secondPassedFlag = false;
    _lastSecondPassed = static_cast<int>(floor(_currentTime - 1));
}

void Timer::update() {
    Entity::update();
    if(!isFinished() && isEnabled()) {
        double now = glfwGetTime();
        double elapsedTime = now -_previousTime;
        _currentTime -= elapsedTime;
        _previousTime = now;
        if(_currentTime <= 0) {
            _finished = true;
        }
        if(_lastSecondPassed > floor(_currentTime)) {
            _secondPassedFlag = true;
            _lastSecondPassed = static_cast<int>(floor(_currentTime));
        }
    }
}

bool Timer::isFinished() {
    return _finished;
}

void Timer::start() {
    Entity::setEnabled(true);
}

void Timer::stop() {
    Entity::setEnabled(false);
}

bool Timer::oneSecondHasPassed() {
    bool flag = _secondPassedFlag;
    if(flag) {
        _secondPassedFlag = false;
    }
    return flag;
}
