/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file CameraWait.hpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */
#ifndef OPENGL_PROJECT_CAMERAWAIT_HPP
#define OPENGL_PROJECT_CAMERAWAIT_HPP

#include <glm/vec3.hpp>
#include "Camera.hpp"
#include "CinematicComponent.hpp"

class CameraWait : public CinematicComponent {
public:
    CameraWait(Camera*, const glm::vec3 & cameraPosition, const glm::vec3 & targetPosition, double time);
    void start() override;
    void update() override;
    void draw() override;

private:
    glm::vec3 _camera;
    glm::vec3 _target;
    double _duration;
    double _startTime;
};


#endif //OPENGL_PROJECT_CAMERAWAIT_HPP
