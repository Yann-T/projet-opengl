/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Camera.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include <glm/ext.hpp>
#include "Camera.hpp"
#include "Engine/Core/Scene.h"
#include "CameraTravel.hpp"
#include "CameraWait.hpp"

Camera::Camera(Scene * scene, const glm::mat4 &mw) : Entity(scene) {
    _view = glm::inverse(mw);
}

Camera::Camera(Scene * scene) : Entity(scene), _view(1), _origin(0), _cinematicQueue() {}

void Camera::update() {
    if (isEnabled()) {
        _scene->setView(_view);
        Entity::update();
    }
}

glm::vec3 Camera::position() const {
    return glm::inverse(_view)*glm::vec4(0,0,0,1);
}

void Camera::lookAt(const glm::vec3 & cameraPosition, const glm::vec3 & targetPosition) {
    _view = glm::lookAt(cameraPosition, targetPosition, {0,1,0});
}

void Camera::pushInCinematicQueue(CinematicComponent * component) {
    component->setEnabled(_cinematicQueue.empty());
    if (_cinematicQueue.empty()) {
	component->start();
    }
    _cinematicQueue.push_back(component);
}

void Camera::nextCinematic() {
    if (!_cinematicQueue.empty()) {
        CinematicComponent *c = _cinematicQueue.front();
        c->setEnabled(false);
        _cinematicQueue.pop_front();
        if (!_cinematicQueue.empty()) {
            _cinematicQueue.front()->start();
        }
    }
}

void Camera::travel(const glm::vec3 & cameraBegin, const glm::vec3 & cameraEnd, const glm::vec3 & targetBegin, const glm::vec3 & targetEnd, double duration) {
    auto * travelComponent = new CameraTravel(this, cameraBegin, cameraEnd, targetBegin, targetEnd, duration);
    pushInCinematicQueue(travelComponent);
    addComponent(travelComponent);
}

void Camera::wait(const glm::vec3 & camera, const glm::vec3 & target, double time) {
    auto * waitComponent = new CameraWait(this, camera, target, time);
    pushInCinematicQueue(waitComponent);
    addComponent(waitComponent);
}
