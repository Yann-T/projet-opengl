/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file CameraTravel.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "CameraTravel.hpp"
#include <GLFW/glfw3.h>

CameraTravel::CameraTravel(Camera * camera, const glm::vec3 &cameraStart, const glm::vec3 &cameraEnd, const glm::vec3 &targetStart,
                           const glm::vec3 &targetEnd, double time)
                           : CinematicComponent(camera),
                             _cameraStart(cameraStart), _cameraEnd(cameraEnd),
                             _targetStart(targetStart), _targetEnd(targetEnd),
                             _travelTime(time), _startTime() {
}

void CameraTravel::start() {
    _startTime = glfwGetTime();
    setEnabled(true);
}

void CameraTravel::update() {
    Camera * camera = dynamic_cast<Camera *>(_entity);
    double currentTime = glfwGetTime() - _startTime;
    if (currentTime > _travelTime) {
        camera->lookAt(_cameraEnd, _targetEnd);
        setEnabled(false);
        camera->nextCinematic();
    } else {
        double lambda = std::min(currentTime / _travelTime, 1.);
        camera->lookAt(linearTravel(lambda, _cameraStart, _cameraEnd),
                linearTravel(lambda, _targetStart, _targetEnd));
    }
}

glm::vec3 CameraTravel::linearTravel(double lambda, const glm::vec3 &start, const glm::vec3 &end) const {
    glm::vec3 pos = glm::vec3(static_cast<float>(1. - lambda)) * start + glm::vec3(static_cast<float>(lambda)) * end;
    return pos;
}

void CameraTravel::draw() {}
