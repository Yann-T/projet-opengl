/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file CameraWait.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include <GLFW/glfw3.h>
#include "CameraWait.hpp"

CameraWait::CameraWait(Camera * camera, const glm::vec3 &cameraPosition, const glm::vec3 &targetPosition, double time)
        : CinematicComponent(camera), _camera(cameraPosition), _target(targetPosition), _duration(time), _startTime(0.) {}

void CameraWait::start() {
    _startTime = glfwGetTime();
}

void CameraWait::update() {
    Camera * camera = dynamic_cast<Camera *>(_entity);
    double currentTime = glfwGetTime() - _startTime;
    if (currentTime > _duration) {
        setEnabled(false);
        camera->nextCinematic();
    } else {
        camera->lookAt(_camera, _target);
    }
}

void CameraWait::draw() {}
