/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file CameraTravel.hpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_CAMERATRAVEL_HPP
#define OPENGL_PROJECT_CAMERATRAVEL_HPP


#include <Engine/Core/Component.h>
#include <glm/vec3.hpp>
#include "Camera.hpp"
#include "CinematicComponent.hpp"

class CameraTravel : public CinematicComponent {
public:
    CameraTravel(Camera * camera,
            const glm::vec3 & cameraStart, const glm::vec3 & cameraEnd,
            const glm::vec3 & targetStart, const glm::vec3 & targetEnd,
            double time);

    void start() override;
    void update() override;
    void draw() override;


private:
    glm::vec3 linearTravel(double lambda, const glm::vec3 &start, const glm::vec3 &end) const;

private:
    glm::vec3 _cameraStart;
    glm::vec3 _cameraEnd;
    glm::vec3 _targetStart;
    glm::vec3 _targetEnd;
    double _travelTime;
    double _startTime;

};


#endif //OPENGL_PROJECT_CAMERATRAVEL_HPP
