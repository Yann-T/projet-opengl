/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file Camera.hpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */
#ifndef OPENGL_PROJECT_CAMERA_HPP
#define OPENGL_PROJECT_CAMERA_HPP

#include <glm/detail/type_mat4x4.hpp>
#include <deque>
#include "Engine/Core/Entity.h"


class CinematicComponent;
class Camera : public Entity {
public:
    explicit Camera(Scene *);
    Camera(Scene *, const glm::mat4 & mw);
    glm::vec3 position() const;
    void lookAt(const glm::vec3 & cameraPosition, const glm::vec3 & targetPosition);
    void travel(const glm::vec3 & cameraBegin, const glm::vec3 & cameraEnd, const glm::vec3 & targetBegin, const glm::vec3 & targetEnd, double duration=0);
    void nextCinematic();
    void wait(const glm::vec3 &camera, const glm::vec3 &target, double time);
    void update() override;

private:
    glm::mat4 _view{};
    glm::vec3 _origin{};
    std::deque<CinematicComponent *> _cinematicQueue;

    void pushInCinematicQueue(CinematicComponent *component);
};


#endif //OPENGL_PROJECT_CAMERA_HPP
