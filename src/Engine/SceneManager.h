/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file SceneManager.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_SCENEMANAGER_H
#define OPENGL_PROJECT_SCENEMANAGER_H


#include "Core/Scene.h"
#include <map>

class SceneManager {
public:
    SceneManager() = default;
    ~SceneManager();
    void addScene(const std::string & name, Scene * scene);
    void changeScene(const std::string & name);
    void changeScene(Scene * newScene);
    void setAudioPlayer(AudioPlayer * audioPlayer);
    Scene * getCurrentScene();

private:
    AudioPlayer * _audioPlayer;
    Scene * _currentScene;
    std::map<std::string, Scene *> _scenes;
};


#endif //OPENGL_PROJECT_SCENEMANAGER_H
