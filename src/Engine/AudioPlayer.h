//
// Created by yann on 13/04/2020.
//

#ifndef OPENGL_PROJECT_AUDIOPLAYER_H
#define OPENGL_PROJECT_AUDIOPLAYER_H

#include <string>
/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file AudioPlayer.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include <soloud.h>
#include <soloud_wav.h>
#include <map>

class AudioPlayer {
public:
    AudioPlayer();
    ~AudioPlayer();

    void addSoundEffect(const std::string& filename, const std::string& name);
    void playSoundEffect(const std::string& name);

private:
    std::map<std::string, SoLoud::Wav *> _registeredEffects;
    SoLoud::Soloud _soloud;
};


#endif //OPENGL_PROJECT_AUDIOPLAYER_H
