/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file InputHandler.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */
#ifndef OPENGL_PROJECT_INPUTHANDLER_H
#define OPENGL_PROJECT_INPUTHANDLER_H


#include <map>
#include "Core/Component.h"
#include <list>

class InputHandler {
public :
    void handleKeyInput (int key);
    void registerComponent(Component* component, std::list<int> keys);
    void setEnabled(bool enabled);
private:
    bool _enabled = true;
    std::multimap<const int, Component*> _keysForComponent;
};


#endif //OPENGL_PROJECT_INPUTHANDLER_H
