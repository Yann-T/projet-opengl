/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file MoveComponent.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef MOVE_COMPONENT_HPP
#define MOVE_COMPONENT_HPP


#include "Engine/Renderable/RenderableComponent.h"
#include "Engine/Core/Component.h"


class MoveComponent : public Component {
public:
    MoveComponent(Entity *entity, RenderableComponent & component);
    void update() override;
    void draw() override;
    void processKey(int key) override;
    int getX() const;
    int getZ() const;
private:
    RenderableComponent & _boundComponent;
    int x, z;
};


#endif //MOVE_COMPONENT_HPP
