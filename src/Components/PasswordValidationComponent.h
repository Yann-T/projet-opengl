/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file PasswordValidationComponent.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef VALIDATION_COMPONENT_HPP
#define VALIDATION_COMPONENT_HPP


#include "Engine/Core/Component.h"
#include "MiniGameModels/PasswordMinigameModel.h"
#include "Engine/MiniGame.h"
#include "Engine/HUD/HUDSpriteSheetComponent.hpp"
#include "MoveComponent.h"

class PasswordValidationComponent : public Component {
    private:
        MoveComponent* moveComponent;
        PasswordMinigameModel* logic;
        MiniGame* miniGame;
        HUDSpriteSheetComponent* hud;
    public:    
        PasswordValidationComponent(Entity* entity, MoveComponent* moveComponent, MiniGame* miniGame, HUDSpriteSheetComponent * hud);
        ~PasswordValidationComponent();
        void update() override;
        void draw() override;
        void processKey(int key) override;
};


#endif //VALIDATION_COMPONENT_HPP
