/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file MoveComponent.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */
#include "MoveComponent.h"
#include "Engine/Core/Entity.h"

MoveComponent::MoveComponent(Entity *entity, RenderableComponent & component) : Component(entity), _boundComponent(component), x(0), z(0) {
}

void MoveComponent::update() {}

void MoveComponent::draw() {}

void MoveComponent::processKey(int key) {
    glm::vec3 translate = glm::vec3(0,0,0);
    if(key == 265) {
        translate += glm::vec3(-0.3,0,1);
        z++;
    } else if(key == 264) {
        translate += glm::vec3(0.3,0,-1);
        z--;
    }  else if(key == 263) {
        translate += glm::vec3(-1,0,0);
        x--;
    }  else if(key == 262) {
        translate += glm::vec3(1,0,0);
        x++;
    }
    _boundComponent.translate(translate);
}

int MoveComponent::getX() const {
    return this->x;
}

int MoveComponent::getZ() const {
    return this->z;
}