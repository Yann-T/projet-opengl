/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file DrinkInputComponent.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */
#include <GLFW/glfw3.h>
#include "DrinkInputComponent.h"

DrinkInputComponent::DrinkInputComponent(Entity *entity, SnackMiniGameModel *model) : Component(entity), _model(model) {}

void DrinkInputComponent::update() {

}

void DrinkInputComponent::draw() {

}

void DrinkInputComponent::processKey(int i) {
    if (GLFW_KEY_KP_0 <= i && i <= GLFW_KEY_KP_9) {
        i = i-GLFW_KEY_KP_0+GLFW_KEY_0;
    }
    _model->addKey(i);
}
