/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file DrinkInputComponent.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_DRINKINPUTCOMPONENT_H
#define OPENGL_PROJECT_DRINKINPUTCOMPONENT_H


#include <Engine/Core/Component.h>
#include <MiniGameModels/SnackMiniGameModel.h>

class DrinkInputComponent : public Component {
public:
    DrinkInputComponent(Entity *entity, SnackMiniGameModel *model) ;

    void update() override;

    void draw() override;

    void processKey(int i) override;
private:
    SnackMiniGameModel *_model;
};


#endif //OPENGL_PROJECT_DRINKINPUTCOMPONENT_H
