/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file PasswordValidationComponent.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "PasswordValidationComponent.h"

void PasswordValidationComponent::update() {}

void PasswordValidationComponent::draw() {}

PasswordValidationComponent::PasswordValidationComponent(Entity* entity, MoveComponent* moveComponent, MiniGame* miniGame, HUDSpriteSheetComponent * hud) :
Component(entity), moveComponent(moveComponent), miniGame(miniGame), hud(hud) {
    this->logic = new PasswordMinigameModel();
}

void PasswordValidationComponent::processKey(int) {
    miniGame->getAudioPlayer()->playSoundEffect("keyboard");
    int x = moveComponent->getX();
    int z = moveComponent->getZ();
    logic->onType(x, z);
    hud->getSpriteAtIndex(logic->getNumberOfCharactersGuessed());
    if(logic->isCorrect()) {
        miniGame->onWin();
    }
}

PasswordValidationComponent::~PasswordValidationComponent() {
    delete this->logic;
}