/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file MainApplication.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include <Scenes/SnackMiniGame.h>
#include <GLFW/glfw3.h>
#include "MainApplication.h"
#include "Scenes/PasswordMiniGame.h"
#include "Engine/SceneManager.h"

MainApplication::MainApplication(int windowWidth, int windowHeight) : GameApplication(windowWidth, windowHeight) {
    preloadMiniGames();
    changeScene("password");
}

void MainApplication::preloadMiniGames() {
    GLFWwindow * window = glfwGetCurrentContext();
    MainApplication &app = *static_cast<MainApplication *>(glfwGetWindowUserPointer(window));
    Scene * password = new PasswordMiniGame();
    Scene * snack = new SnackMiniGame();
    password->preload();
    snack->preload();
    password->setInputHandler(new InputHandler());
    snack->setInputHandler(new InputHandler());
    app._sceneManager->addScene("password", password);
    app._sceneManager->addScene("snack", snack);
}
