/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file PasswordMinigameModel.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "PasswordMinigameModel.h"

PasswordMinigameModel::PasswordMinigameModel() : charsGuessed(0) {
    this->password = "ENSI";
}

bool PasswordMinigameModel::onType(int x, int z) {
    char keyboardKey = '\0';
    if(z == 0) {
        if(x == -5) {
            keyboardKey = 'N';
        } else if(x == -6) {
            keyboardKey = 'B';
        } else if(x == -7) {
            keyboardKey = 'V';
        } else if(x == -8) {
            keyboardKey = 'C';
        } else if(x == -9) {
            keyboardKey = 'X';
        } else if(x == -10) {
            keyboardKey = 'W';
        }
    } else if(z == 1) {
        if(x == -1) {
            keyboardKey = 'M';
        } else if(x == -2) {
            keyboardKey = 'L';
        } else if(x == -3) {
            keyboardKey = 'K';
        } else if(x == -4) {
            keyboardKey = 'J';
        } else if(x == -5) {
            keyboardKey = 'H';
        } else if(x == -6) {
            keyboardKey = 'G';
        } else if(x == -7) {
            keyboardKey = 'F';
        } else if(x == -8) {
            keyboardKey = 'D';
        } else if(x == -9) {
            keyboardKey = 'S';
        } else if(x == -10) {
            keyboardKey = 'Q';
        }
    } else if(z == 2) {
        if(x == -1) {
            keyboardKey = 'P';
        } else if(x == -2) {
            keyboardKey = 'O';
        } else if(x == -3) {
            keyboardKey = 'I';
        } else if(x == -4) {
            keyboardKey = 'U';
        } else if(x == -5) {
            keyboardKey = 'Y';
        } else if(x == -6) {
            keyboardKey = 'T';
        } else if(x == -7) {
            keyboardKey = 'R';
        } else if(x == -8) {
            keyboardKey = 'E';
        } else if(x == -9) {
            keyboardKey = 'Z';
        } else if(x == -10) {
            keyboardKey = 'A';
        }
    }
    if(keyboardKey) {
        typed += keyboardKey;
        charsGuessed = checkWords();
        return true;
    }
    return false;
}

int PasswordMinigameModel::checkWords() {
    for(int i = 0; i < (int)typed.size(); i++) {
        if(typed[i] != password[i]) {
            typed.clear();
            return 0;
        }
    }
    return (int)typed.size();
}

bool PasswordMinigameModel::isCorrect() const {
    return typed == password;
}

int PasswordMinigameModel::getNumberOfCharactersGuessed() const {
    return charsGuessed;
}