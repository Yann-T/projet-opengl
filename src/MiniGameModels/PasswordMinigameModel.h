/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file PasswordMinigameModel.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef KEYBOARD_LOGIC_HPP
#define KEYBOARD_LOGIC_HPP
 
#include <string>

class PasswordMinigameModel {
    private:
        std::string typed;
        std::string password;
        int charsGuessed;
        int checkWords();

    public:
        bool onType(int x, int z);
        bool isCorrect() const;
        int getNumberOfCharactersGuessed() const;
        PasswordMinigameModel();
};


#endif //KEYBOARD_LOGIC_HPP
