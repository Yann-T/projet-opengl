/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file SnackMiniGameModel.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */


#include <string>
#include "SnackMiniGameModel.h"
#include "../Scenes/SnackMiniGame.h"

SnackMiniGameModel::SnackMiniGameModel(SnackMiniGame *miniGame, int numberOfAttempts) : _miniGame(miniGame),
                                                                                        _numberOfAttemptsLeft(numberOfAttempts) {
    _currentInput = std::string();
}

void SnackMiniGameModel::addItemToOrder(const std::string &newItemToOrder) {
    _itemsToOrder.push_back(newItemToOrder);
}

bool SnackMiniGameModel::checkGuess(const std::string &itemTyped) {
    for (auto i = _itemsToOrder.begin(); i != _itemsToOrder.end(); ++i) {
        if (itemTyped == *i) {
            _itemsToOrder.erase(i);
            return true;
        }
    }
    return false;
}

void SnackMiniGameModel::addKey(const int numberPressed) {
    _currentInput.push_back((char) numberPressed);
    if (_currentInput.size() > 2) {
        handleCompletedGuess();
        _currentInput = std::string();
    }
}

void SnackMiniGameModel::handleCompletedGuess() {
    if (checkGuess(_currentInput)) {
        _miniGame->onRightAnswer();

        if (_itemsToOrder.empty()) {
            _miniGame->onWin();
        }
    } else {
        _miniGame->onWrongAnswer();
        _numberOfAttemptsLeft--;

        if (_numberOfAttemptsLeft < 1) {
            _miniGame->onFailed();
        }
    }
}

