/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file SnackMiniGameModel.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_SNACKMINIGAMEMODEL_H
#define OPENGL_PROJECT_SNACKMINIGAMEMODEL_H

#include <vector>
#include <string>

class SnackMiniGame;

class SnackMiniGameModel {
public:
    explicit SnackMiniGameModel(SnackMiniGame * miniGame, int numberOfAttempts);
    void addKey(int numberPressed);

    void addItemToOrder(const std::string& newItemToOrder);

private:
    bool checkGuess(const std::string & itemTyped);
    void handleCompletedGuess();

private:
    SnackMiniGame * _miniGame;
    int _numberOfAttemptsLeft;
    std::string _currentInput;
    std::vector<std::string> _itemsToOrder;
};


#endif //OPENGL_PROJECT_SNACKMINIGAMEMODEL_H
