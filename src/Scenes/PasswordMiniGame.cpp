/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file PasswordMiniGame.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#include "PasswordMiniGame.h"
#include "SnackMiniGame.h"
#include <glm/ext.hpp>
#include "Components/KeyboardModelComponent.h"
#include "Components/HandModelComponent.h"
#include "Components/MoveComponent.h"
#include "Components/PasswordValidationComponent.h"
#include "Engine/SceneManager.h"
#include "Engine/Camera/Camera.hpp"
#include "Engine/HUD/HUD.hpp"

PasswordMiniGame::PasswordMiniGame() : MiniGame(16, {0.8,0.7,0.6,1}), _meshes() {
}

void PasswordMiniGame::preload() {
    const auto pi = glm::pi<float>();
    glm::mat4 mwHand(1);
    mwHand = glm::scale(mwHand, glm::vec3(-0.1,0.1,0.1));
    mwHand = glm::rotate(mwHand, pi, {0, 1, 0});
    mwHand = glm::translate(mwHand, {3, 2, -10});
    _meshes["hand"] = new HandModelComponent(nullptr, mwHand);
}

void PasswordMiniGame::initialize() {
    MiniGame::initialize();

    auto * passwordGuess = new HUDSpriteSheetComponent(_hud, "textures/Password/password.png",
            {0,-0.8}, 1, 5);
    auto * passwordInstruction = new HUDImageComponent(_hud, "textures/Password/password_instruction.png",
    {0,0.8});
    _hud->addComponent(passwordGuess);
    _hud->addComponent(passwordInstruction);

    const auto pi = glm::pi<float>();

    auto * camera = new Camera(this);
    glm::vec3 begin = {-8, 2., 0.};
    glm::vec3 targetBegin {-8, 3, -10};
    camera->wait(begin, targetBegin, 5);
    camera->travel(begin, {-0.3,1,.005},
            targetBegin, {-0.3, -1., 0.}, 1);

    glm::mat4 mwTable(1);
    mwTable = glm::scale(mwTable, {2,2,2});
    mwTable = glm::translate(glm::mat4(1), {1,-0.52,-1})*mwTable;
    mwTable = glm::rotate(mwTable, pi/2, {0, 1,0});

    auto * entityTable = new Entity(this);
    entityTable->addComponent(new RenderableComponent(entityTable, "meshes/Password/Table/table.obj", mwTable));


    glm::mat4 mwKeyboard(1);
    mwKeyboard = glm::rotate(mwKeyboard, -pi / 2, {0, 1, 0});
    mwKeyboard = glm::translate(mwKeyboard, {0, -0.2, -0.3});

    auto * entityKeyboard = new Entity(this);
    entityKeyboard->addComponent(new KeyboardModelComponent(entityKeyboard, mwKeyboard));

    glm::mat4 mwMonitor(1);
    mwMonitor = glm::rotate(mwMonitor, -pi/2, {0,1, 0});
    mwMonitor = glm::translate(mwMonitor, {-1.3, 1.1, 0});
    auto * entityMonitor = new Entity(this);
    entityMonitor->addComponent(new RenderableComponent(entityMonitor, "meshes/Password/ComputerMonitor/ComputerMonitor.obj", mwMonitor));

    glm::mat4 mwWhiteboard(1);
    mwWhiteboard = glm::scale(mwWhiteboard, glm::vec3(0.7));
    mwWhiteboard = glm::rotate(mwWhiteboard, -pi/2, {0,1, 0});
    mwWhiteboard = glm::translate(mwWhiteboard, {-10,0,13});
    auto * entityWhiteboard = new Entity(this);
    entityWhiteboard->addComponent(new RenderableComponent(entityWhiteboard, "meshes/Password/Whiteboard/Whiteboard.obj", mwWhiteboard));

    auto * entityHand = new Entity(this);
    entityHand->addComponent(_meshes["hand"]);
    _meshes["hand"]->setEntity(entityHand);
    entityHand->addComponent(new MoveComponent(entityHand, *entityHand->getComponent<HandModelComponent>()));

    auto * entityValidation = new Entity(this);
    entityValidation->addComponent(new PasswordValidationComponent(entityHand, entityHand->getComponent<MoveComponent>(), this, passwordGuess));


    auto * inputHandler = new InputHandler();
    inputHandler->registerComponent(entityHand->getComponent<MoveComponent>(), {262, 263, 264, 265}); /* 262:RIGHT, 263:LEFT, 264:DOWN, 265:UP */
    inputHandler->registerComponent(entityValidation->getComponent<PasswordValidationComponent>(), {257}); /* 257:ENTER */
    setInputHandler(inputHandler);

    addEntity(camera);
    addEntity(entityHand);
    addEntity(entityKeyboard);
    addEntity(entityMonitor);
    addEntity(entityWhiteboard);
    addEntity(entityTable);

    glEnable(GL_DEPTH_TEST);
}

void PasswordMiniGame::onWin() {
    getInputHandler()->setEnabled(false);
    getAudioPlayer()->playSoundEffect("win");
    getSceneManager()->changeScene("snack");
}

void PasswordMiniGame::onFailed() {
    getAudioPlayer()->playSoundEffect("fail");
    getSceneManager()->changeScene("snack");
}

void PasswordMiniGame::onTimerFinished() {
    onFailed();
}
