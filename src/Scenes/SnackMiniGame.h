/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file SnackMiniGame.h
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */

#ifndef OPENGL_PROJECT_SNACKMINIGAME_H
#define OPENGL_PROJECT_SNACKMINIGAME_H

#include "../Engine/AudioPlayer.h"
#include "../Engine/MiniGame.h"
#include <vector>

class SnackMiniGameModel;
class RenderableComponent;

class SnackMiniGame : public MiniGame{
public:
    SnackMiniGame();
    void onRightAnswer();
    void onWrongAnswer();
    
public:
    void preload() override;
    void initialize() override;
    void onWin() override;
    void onFailed() override;
    void onTimerFinished() override;

private:
    SnackMiniGameModel * _model;
    std::vector<RenderableComponent *> _meshes;
};


#endif //OPENGL_PROJECT_SNACKMINIGAME_H
