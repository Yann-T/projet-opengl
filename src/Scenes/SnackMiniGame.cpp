/* -*- c-basic-offset: 4 -*-
 *
 * ENSICAEN
 * 6 Boulevard Marechal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @file SnackMiniGame.cpp
 * @author Samuth Benjamin <benjamin.samuth@ecole.ensicaen.fr>
 * @author Trupot Yann <yann.trupot@ecole.ensicaen.fr>
 * @author Robert Nicolas <nicolas.robert@ecole.ensicaen.fr>
 * @author Larzilliere Alexandre <alarzillere@ecole.ensicaen.fr>
 * @author Missing-djawad Tanilola <tanilola.missing-djawad@ecole.ensicaen.fr>
 * @version 1.0
 * @date 16-04-2020
 * @copyright Copyright (c) 2020
 */


#include "SnackMiniGame.h"
#include <ctime>
#include "MiniGameModels/SnackMiniGameModel.h"
#include "Engine/Camera/Camera.hpp"
#include "Engine/HUD/HUDImageComponent.hpp"
#include "Engine/HUD/HUD.hpp"
#include "Components/DrinkModelComponent.h"
#include <glm/ext.hpp>
#include "Engine/SceneManager.h"
#include "PasswordMiniGame.h"
#include <Components/DrinkInputComponent.h>
#include <Application/MainApplication.h>

SnackMiniGame::SnackMiniGame() : MiniGame(15, {0.9, 0.8, 0.8, 1}), _meshes(3) {
    _model = new SnackMiniGameModel(this, 3);
}

void SnackMiniGame::preload() {
    std::map<std::string, std::string> keyPaths;
    keyPaths.insert(std::make_pair("012", "meshes/Drinks/Cokecan_textured.obj"));
    keyPaths.insert(std::make_pair("115", "meshes/Drinks/pepsi.obj"));
    keyPaths.insert(std::make_pair("286", "meshes/Drinks/beer.obj"));
    auto it = keyPaths.begin();

    std::srand(static_cast<unsigned int>(std::time(NULL)));
    std::advance(it, std::rand() % keyPaths.size());
    _model->addItemToOrder(it->first);
    auto mesh0 = it->second;
    it = keyPaths.begin();
    std::advance(it, std::rand() % keyPaths.size());
    _model->addItemToOrder(it->first);
    auto mesh1 = it->second;
    it = keyPaths.begin();
    std::advance(it, std::rand() % keyPaths.size());
    _model->addItemToOrder(it->first);
    auto mesh2 = it->second;

    const auto pi = glm::pi<float>();
    glm::mat4 mwDrink(1);
    mwDrink = glm::scale(mwDrink, glm::vec3(0.3));
    mwDrink = glm::rotate(mwDrink, -pi / 2, {0, 1, 0});
    _meshes[0] = new DrinkModelComponent(nullptr, mesh0, mwDrink);
    _meshes[1] = new DrinkModelComponent(nullptr, mesh1, mwDrink);
    _meshes[2] = new DrinkModelComponent(nullptr, mesh2, mwDrink);
}

void SnackMiniGame::initialize() {
    MiniGame::initialize();

    auto * snackInstruction = new HUDImageComponent(_hud, "textures/Snack/snack_instruction.png",
            {0,0.8});
    _hud->addComponent(snackInstruction);

    const auto pi = glm::pi<float>();

    auto * camera = new Camera(this);

    glm::vec3 begin = {-6, 0, 0};
    glm::vec3 targetBegin {0, 0, 0};
    camera->wait(begin, targetBegin, 4);
    camera->travel(begin, {-6,0,0},
                   targetBegin, {-6, 0, 5}, 2);

    glm::mat4 mwVendingMachine(1);
    mwVendingMachine = glm::scale(mwVendingMachine, glm::vec3(0.3));
    mwVendingMachine = glm::rotate(mwVendingMachine, pi/2, {0, 1, 0});
    mwVendingMachine = glm::translate(mwVendingMachine, {-8, 0, -20});

    auto * entityDrink1 = new Entity(this);
    entityDrink1->addComponent(_meshes[0]);
    _meshes[0]->setEntity(entityDrink1);
    _meshes[0]->translate({-6,0,0});

    auto * entityDrink2 = new Entity(this);
    entityDrink2->addComponent(_meshes[1]);
    _meshes[1]->setEntity(entityDrink2);
    _meshes[1]->translate({0,0,0});

    auto * entityDrink3 = new Entity(this);
    entityDrink3->addComponent(_meshes[2]);
    _meshes[2]->setEntity(entityDrink3);
    _meshes[2]->translate({6,0,0});

    auto * entityVendingMachine = new Entity(this);
    entityVendingMachine->addComponent(new DrinkModelComponent(entityVendingMachine, "meshes/Vending/vending.obj", mwVendingMachine));
    entityVendingMachine->addComponent(new DrinkInputComponent(entityVendingMachine, _model));


    auto * inputHandler = new InputHandler();
    inputHandler->registerComponent(entityVendingMachine->getComponent<DrinkInputComponent>(),
            {48, 49, 50, 51, 52, 53, 54, 55, 56, 57,
             320, 321, 322, 323, 324, 325, 326, 327, 328, 329});
    setInputHandler(inputHandler);

    addEntity(camera);
    addEntity(entityDrink1);
    addEntity(entityDrink2);
    addEntity(entityDrink3);
    addEntity(entityVendingMachine);

    glEnable(GL_DEPTH_TEST);
}

void SnackMiniGame::onWin() {
    getAudioPlayer()->playSoundEffect("win");
    MainApplication::preloadMiniGames();
    getSceneManager()->changeScene("password");
    // transition to a "You win" scene ?
}

void SnackMiniGame::onFailed() {
    getAudioPlayer()->playSoundEffect("fail");
    MainApplication::preloadMiniGames();
    getSceneManager()->changeScene("password");
    // We will either fail by running out of time or by making too much errors
}

void SnackMiniGame::onTimerFinished() {
    onFailed();
}

void SnackMiniGame::onRightAnswer() {
    getAudioPlayer()->playSoundEffect("pass");
}

void SnackMiniGame::onWrongAnswer() {
    getAudioPlayer()->playSoundEffect("fail2");
}
