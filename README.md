Escape from ENSI B
====

# Authors
* Alexandre LARZILLIERE
* Tanilola MISSING-DJAWAD
* Nicolas ROBERT
* Benjamin SAMUTH
* Yann TRUPOT

# Requirements
* CMake 2.8
* OpenGL 4.1+
* GLFW 3.0
* GLM 0.9.2
* SDL2 (audio)

# Build
To compile the project, open a command terminal in the project directory:
```shell script
mkdir build; cd build; make
```

---

# Run
The program can be executed without any argument:
```shell script
./opengl_project
```

---

# Demo

![Password](concept-art/screenshot-password1.png)

![Password](concept-art/screenshot-snack1.jpg)

---

# Controls

### Keyboard (Global mappings)
* Q *(A in AZERTY)*: Quit

### Keyboard (mini-game #1)
* Directional arrows : Move the hand
* Enter : Confirm a key

### Keyboard (mini-game #2)
* 1-9 *(Typewriter or Numeric Keypad)* : Input a digit

---

# License

Copyright © 2020 ENSICAEN

*ENSICAEN
6 Boulevard Maréchal Juin
F-14050 Caen Cedex*

This software is owned by ENSICAEN students.
No portion of this software may be reproduced, copied
or revised without written permission of the authors.