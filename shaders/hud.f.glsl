#version 410

in vec2 uv;

uniform sampler2D colormap;

out vec4 fragColor;

void main()
{
    vec4 color = texture(colormap, uv);
    if(color.a==0.0) discard;
    fragColor=color;
}
