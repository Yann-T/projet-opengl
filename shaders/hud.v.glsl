#version 410

layout(location = 0) in vec2 vertexPosition;
layout(location = 1) in vec2 vertexUV;

uniform float aspect;
uniform vec2 origin;
uniform float size;
out vec2 uv;

void main()
{
    vec2 transformVertexPosition = origin+vec2(size*vertexPosition[0]/aspect, size*vertexPosition[1]);
    vec4 positionH=vec4(vec2(transformVertexPosition[0], transformVertexPosition[1]),0,1);
    gl_Position = positionH;
    uv = vertexUV;
}
