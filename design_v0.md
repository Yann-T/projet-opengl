# Design V0

## Team

**Team Leader** : Samuth Benjamin  
**Developper 1** : Robert Nicolas  
**Developper 2** : Samuth Benjamin  
**Developper 3** : Missing-djawad Tanilola  
**Developper 4** : Larzilliere Alexandre  
**Developper 5** : Trupot Yann  
**Asset designer 1** : Samuth Benjamin  
**Asset designer 2** : Robert Nicolas   
**Main integrator** : Trupot Yann  


## Game overview

**Goal** : Arrive on time to lessons at ENSI B then leave the ENSI B building in order to ~~skip classes~~ escape.   
**Gameplay** : Sequences of randomized short mini-games, with a harder puzzle at the end of each sequence.  
**Controls** : Keyboard or mouse, may vary from a mini-game to another. 

The gameplay design is based around the game series "WarioWare".   
[WarioWare, Inc (GBA Copyright 2003 Nintendo)](https://youtu.be/XLg1zLXTnL0?t=3062)

## Detailed description

Mini-games ask for basic input, like a mouse click or a spacebar press for example. The player have a limited amount of time to complete each minigame, about 5 to 10 seconds.
After each boss phase, every mini-game is then accelerated so they would require focus and reactivity.

Mini-games can be for example:
*  Wake your classmate up : The player has to click a 10 times, a character will proceed to shake his classmate at each click.
*  Copy code lines : The player has to type on his keyboard what is written on the screen on a fictive terminal.
*  Order a snack at the dispenser : The player has to enter the code on their numeric pad, matching the snack indicated on screen with what is in the dispenser.
*  Select « Ubuntu » on the Dual Boot screen : The player has to press down once (and only once) to select « Ubuntu »
*  Sign the attendance sheet : The player has to draw something with his cursor in a small rectangle. Holding the click will leave a mark on the paper.
*  ...

**For this v0, we plan to implement 2 mini-games using a dedicated engine that we will develop with OpenGL.**

Those mini-games should be entirely made from the engine as a proof of concept.

### Engine
The engine will be a facade for the team in order to factor the most common elements of each mini-games, especially every interaction with the OpenGL and GLFW commands.
Ultimately, this would help us implement new mini-games more easily in the long run (*if there were further versions*).

**Features**
- Core: Based around the [Entity-Component](https://en.wikipedia.org/wiki/Entity_component_system) architectural pattern.
- 3D models import and render: Wavefront (.obj) support with TinyObjLoader.
- HUD: Draw 2D images on the screen, whole or sprite sheets with stb_image.
- Timer: Countdowns with GLFW.
- Input handling: Keyboard input with GLFW.
- Mini-game main class

### Mini-game 1: Password
The player needs to enter a four letter password on a keyboard, while moving a hand over its keys inside the game.
A hint will be displayed on a whiteboard beforehand, and the player will have to guess the password.

![Minigame](concept-art/password1.jpg)

![Minigame](concept-art/password2.jpg)

**Technical features**
- 3D models: Wavefront (.obj) type. Whiteboard, keyboard, hand…
- Custom textures: Password hint on the whiteboard, keyboard keys (AZERTY).
- HUD: Password guess
- Keyboard inputs to handle: Directional arrows, [ENTER] key to push a key on the "virtual" keyboard.
- Camera panning: From the whiteboard to the top of the keyboard.

### Mini-game 2: Snack
Three snacks will be shown to the player for a short amount of time.
The player will have to remember those items and enter their code on a numeric pad of a vending machine.

**Technical features**
- 3D models: Wavefront (.obj) type. Soda cans, snacks, vending machine.
- Custom textures: items code.
