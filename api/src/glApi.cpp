#include <fstream>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

#include "glApi.hpp"
#include "utils.hpp"

Buffer::Buffer(GLenum target) : m_location(0), m_target(target), m_attributeSize(0)
{
  glGenBuffers(1 , &m_location);
}

Buffer::~Buffer()
{
  glDeleteBuffers(1, &m_location);
}

void Buffer::bind() const
{
  glBindBuffer(m_target, m_location);
}

void Buffer::unbind() const
{
  glBindBuffer(m_target, 0);
}

uint Buffer::attributeCount() const
{
  return m_attributeCount;
}

GLenum Buffer::attributeType() const
{
  return m_attributeType;
}

uint Buffer::attributeSize() const
{
  return m_attributeSize;
}

VAO::VAO(uint nbVBO) : m_location(0), m_vbos(nbVBO), m_ibo(GL_ELEMENT_ARRAY_BUFFER)
{
  for (auto & vbo : m_vbos) {
    vbo = std::shared_ptr<Buffer>(new Buffer(GL_ARRAY_BUFFER));
  }
  assert(nbVBO <= 16); // You may want to replace 16 by the real hardware limitation

  glGenVertexArrays(1, &m_location);
}

VAO::~VAO()
{
  glDeleteVertexArrays(1, &m_location);
}

void VAO::bind() const
{
  glBindVertexArray(m_location);
}

void VAO::unbind() const
{
  glBindVertexArray(0);
}

void VAO::encapsulateVBO(unsigned int attributeIndex) const
{
    bind();
    glEnableVertexAttribArray(attributeIndex);
    m_vbos[attributeIndex]->bind();
    glVertexAttribPointer(attributeIndex, m_vbos[attributeIndex]->attributeSize(), m_vbos[attributeIndex]->attributeType(), GL_FALSE, 0, 0);
    m_vbos[attributeIndex]->unbind();
    unbind();
}

std::shared_ptr<VAO> VAO::makeSlaveVAO() const
{
  unsigned int nbVBO = m_vbos.size();
  std::shared_ptr<VAO> slave(new VAO(nbVBO));
  slave->m_vbos = m_vbos;
  slave->bind();
  for (unsigned int attributeIndex = 0; attributeIndex < nbVBO; attributeIndex++) {
    slave->encapsulateVBO(attributeIndex);
  }
  slave->unbind();
  return slave;
}

void VAO::draw(GLenum mode) const
{
    bind();
    glDrawElements(mode, m_ibo.attributeCount(), m_ibo.attributeType(), 0);
    unbind();
}
/**
 * @brief Constructor from a filename
 * @param type Vertex or Fragment shader
 * @param filename the name of the source file
 *
 * @note PA1: At construction, the following actions must take place:
 * 	- GPU memory allocation
 * 	- reading the content of the file named @p filename (see utils.hpp ::fileContent)
 * 	- setting the source code of the shader
 *  - compiling the shader
 */
Shader::Shader(GLenum type, const std::string & filename) : m_location(0)
{
    //assert(fileExists(filename));
    std::string source = fileContent(filename);
    const char* source_cstr = source.c_str();
    m_location = glCreateShader(type);
    GLint size = source.size();
    glShaderSource(m_location, 1, (const GLchar**)&source_cstr, &size);
    glCompileShader(m_location);
}

Shader::~Shader()
{
    glDeleteShader(m_location);
}

uint Shader::location() const
{
    return m_location;
}

Program::Program(const std::string & vname, const std::string & fname) : m_location(0), m_vshader(GL_VERTEX_SHADER, vname), m_fshader(GL_FRAGMENT_SHADER, fname)
{
    m_location = glCreateProgram();
    glAttachShader(m_location, m_vshader.location());
    glAttachShader(m_location, m_fshader.location());
    glLinkProgram(m_location);

    glDetachShader(m_location, m_fshader.location());
    glDetachShader(m_location, m_vshader.location());
}

Program::~Program()
{
    glDeleteProgram(m_location);
}

void Program::bind() const
{
    glUseProgram(m_location);
}

void Program::unbind() const
{
    glUseProgram(0);
}

bool Program::getUniformLocation(const std::string & name, int & location) const
{
  location = glGetUniformLocation(m_location, name.c_str());
  return location != -1;
}
template <> void Program::uniformDispatcher(int location, const int & val)
{
  glUniform1i(location, val);
}

template <> void Program::uniformDispatcher(int location, const uint & val)
{
  glUniform1ui(location, val);
}

template <> void Program::uniformDispatcher(int location, const float & val)
{
  glUniform1f(location, val);
}

template <> void Program::uniformDispatcher(int location, const glm::vec2 & val)
{
  glUniform2fv(location, 1, glm::value_ptr(val));
}

template <> void Program::uniformDispatcher(int location, const glm::vec3 & val)
{
  glUniform3fv(location, 1, glm::value_ptr(val));
}

template <> void Program::uniformDispatcher(int location, const glm::vec4 & val)
{
  glUniform4fv(location, 1, glm::value_ptr(val));
}

template <> void Program::uniformDispatcher(int location, const glm::mat2 & val)
{
  glUniformMatrix2fv(location, 1, false, glm::value_ptr(val));
}

template <> void Program::uniformDispatcher(int location, const glm::mat3 & val)
{
  glUniformMatrix3fv(location, 1, false, glm::value_ptr(val));
}

template <> void Program::uniformDispatcher(int location, const glm::mat4 & val)
{
  glUniformMatrix4fv(location, 1, false, glm::value_ptr(val));
}

bool Program::bound() const
{
  int currentProgram;
  glGetIntegerv(GL_CURRENT_PROGRAM, &currentProgram);
  return m_location == (GLuint)currentProgram;
}

Texture::Texture(GLenum target) : m_location(0), m_target(target)
{
    glGenTextures(1, &m_location);
}

Texture::~Texture()
{
    glDeleteTextures(1, &m_location);
}

void Texture::bind() const
{
    glBindTexture(m_target, m_location);
}

void Texture::unbind() const
{
    glBindTexture(m_target, 0);
}

template <> void Texture::setData<GLubyte>(const Image<GLubyte> & image, bool mipmaps) const
{
    bind();
    GLenum internal_format=GL_RED;
    if(image.channels == 2) {
        internal_format = GL_RG;
    }
    if(image.channels == 3) {
        internal_format = GL_RGB;
    }
    if(image.channels == 4) {
        internal_format = GL_RGBA;
    }
    if(m_target == GL_TEXTURE_2D) {
        glTexImage2D(m_target, 0, internal_format, image.width, image.height, 0, internal_format, GL_UNSIGNED_BYTE, image.data);
    }
    if(m_target == GL_TEXTURE_3D) {
        glTexImage3D(m_target, 0, internal_format, image.width, image.height, image.depth, 0, internal_format, GL_UNSIGNED_BYTE, image.data);
    }
    if(mipmaps) {
        glGenerateMipmap(m_target);
    }
    unbind();
}

Sampler::Sampler(int texUnit) : m_location(0), m_texUnit(texUnit)
{
    glGenSamplers(1, &m_location);
}

Sampler::~Sampler()
{
    glDeleteSamplers(1, &m_location);
}

void Sampler::bind() const
{
    glBindSampler(m_texUnit, m_location);
}

void Sampler::unbind() const
{
    glBindSampler(m_texUnit, 0);
}

void Sampler::attachToProgram(const Program & prog, const std::string & samplerName, BindOption bindOption) const
{
    if(bindOption == BindOption::BindUnbind) {
        prog.bind();
    }

    prog.setUniform(samplerName, m_texUnit);

    if(bindOption == BindOption::BindUnbind) {
        prog.unbind();
    }
}

void Sampler::attachTexture(const Texture & texture) const
{
  glActiveTexture(GL_TEXTURE0+m_texUnit);
  texture.bind();
}

template <> void Sampler::setParameter<int>(GLenum paramName, const int & value) const
{
  glSamplerParameteri(m_location, paramName, value);
}

template <> void Sampler::setParameter<float>(GLenum paramName, const float & value) const
{
  glSamplerParameterf(m_location, paramName, value);
}

void Sampler::enableAnisotropicFiltering() const
{
  GLfloat maxAniso = 1.0f;
  // get the (hardware specific) maximum admissible anisotropy ratio
  glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &maxAniso);
  setParameter(GL_TEXTURE_MAX_ANISOTROPY_EXT, maxAniso);
}
